# TRAIN ATV API

TRAIN ATV API provides the api end points in the form of POST requests by which the client can access the ATV to verify the issuers.


# Requirements

* Java 1.8 or newer
* Maven ~3.6.0
* Tomcat 9 or higher

# How to use it

* [Swagger](https://app.swaggerhub.com/apis/train8/atv/1.0.0)

## Example Usage

```POST Request
{
    "Issuer": "did:exa:12345678",
    "Trust_Scheme_Pointer": "test-did.example.com"
}
```
# Licence
* Apache License 2.0 (see [LICENSE](./LICENSE))




