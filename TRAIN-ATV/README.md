# TRAIN Automatic Trust Verifier

TRAIN ATV is used to verify the trust of the verifiable credentials. In interoperability use case ATV verifies the trust of the issuer of the credentials.

# Requirements

* Java 1.8 or newer
* Maven ~3.6.0
* IAIK libraries (see [pom.xml](./pom.xml#L182-211))
  * ([Library overview](https://jce.iaik.tugraz.at/sic/Products), [free opensource license available](https://jce.iaik.tugraz.at/sic/Sales/Licences/License_for_Open_Source_Projects)) 
* Internet access 
* LIGHTest Trust Infrastructure
  * Trust Scheme Publication Authority
  * Zone Manager


# How to use it

* Clients:
  * [LocalATVClient](src/main/java/eu/lightest/verifier/client/SSIClient.java) 
* Configuration: [atv.properties](src/main/resources/atv.properties)
* Log Config:  [log4j.properties](src/main/resources/log4j.properties)

 
## Maven

You can use the ATV library as a Maven dependency

```xml
<dependency>
    <groupId>eu.train</groupId>
    <artifactId>atv</artifactId>
    <version>1.x.x</version>
</dependency>
```
# Licence
* Apache License 2.0 (see [LICENSE](./LICENSE))
