accept(Form) :-
  extract(Form, format, ssi),
  extract(Form, bid, Bid),  Bid <= 1500,
  extract(Form, certificate, Certificate),
  extract(Certificate, format, eIDAS_qualified_certificate),
  extract(Certificate, pubKey, PK),
  verify_signature(Form, PK),
  check_eIDAS_qualified(Certificate).

check_eIDAS_qualified(Certificate) :-
  extract(Certificate, issuer, IssuerCertificate),
  extract(IssuerCertificate, trustScheme, TrustSchemeClaim),



