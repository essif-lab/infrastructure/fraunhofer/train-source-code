package eu.lightest.verifier.client;

import eu.lightest.verifier.exceptions.DNSException;
import eu.lightest.verifier.model.report.BufferedStdOutReportObserver;
import eu.lightest.verifier.model.report.Report;
import eu.lightest.verifier.model.trustscheme.TrustScheme;
import eu.lightest.verifier.model.trustscheme.TrustSchemeClaim;
import eu.lightest.verifier.model.trustscheme.TrustSchemeFactory;
import eu.lightest.verifier.wrapper.XMLUtil;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class SSIClient {

    public class VerificationResultClass {
        String ReceivedTrustSchemePointer = "";
        boolean FindingCorrespondingTrustSchemeInitiated = false;
        String FoundCorrespondingTrustScheme = "";
        boolean TrustListDiscoveryInitiated = false;
        String TrustListFoundAndLoaded = "";
        String FoundIssuer = "";
        boolean VerifyIssuer = false;
        boolean VerificationSuccessful = false;
    }

    public class SSIResponse {

        public boolean VerificationStatus = false;
        public VerificationResultClass VerificationResult = new VerificationResultClass();

    }

    public SSIResponse VerifyIdentity(String issuer, String claim)
    {
        Report report = new Report();
        BufferedStdOutReportObserver reportBuffer = new BufferedStdOutReportObserver();
        report.addObserver(reportBuffer);

        report.addLine("Checking Identity");
        report.addLine("DID: " + issuer);
        report.addLine("Claim: " + claim);

        System.out.println("DID: " + issuer);
        System.out.println("Claim: " + claim);

        TrustSchemeClaim TSClaim = new TrustSchemeClaim(claim);
        TrustSchemeFactory TSFactory = new TrustSchemeFactory();

        boolean bVerificationStatus = false;

        SSIResponse resp = new SSIResponse();
        resp.VerificationResult.ReceivedTrustSchemePointer = claim;
        resp.VerificationResult.FindingCorrespondingTrustSchemeInitiated = true;


        try {
            TrustScheme scheme = TSFactory.createTrustScheme(TSClaim, report);

            if(scheme == null)
                throw new IOException("Did not find TrustScheme / TrustList");

            resp.VerificationResult.FoundCorrespondingTrustScheme = scheme.getSchemeIdentifierCleaned();
            resp.VerificationResult.TrustListDiscoveryInitiated = true;
            resp.VerificationResult.TrustListFoundAndLoaded = scheme.getTSLlocation();

            report.addLine("TrustScheme Hostname: " + scheme.getSchemeIdentifierCleaned());
            report.addLine("TrustList Location: " + scheme.getTSLlocation());


            XMLUtil util = new XMLUtil(scheme.getTSLcontent());
            String DID = util.getElementByXPath("//TrustServiceProviderList/TrustServiceProvider/TSPInformation/DIDName/Name[1]/text()");

            report.addLine("DID (extracted): " + DID);

            if (DID.equals(issuer))
            {
                resp.VerificationResult.FoundIssuer = issuer;
                resp.VerificationResult.VerifyIssuer = true;
                resp.VerificationResult.VerificationSuccessful = true;
                resp.VerificationStatus = true;
            }
        }catch (Exception e)
        {

            report.addLine("Checking identity failed: ");
            report.addLine(e.getMessage());

        }
        reportBuffer.print();






        return resp;
    }
}
